package sheridan;

import java.util.logging.Logger;

import sun.util.logging.resources.logging;

public class PrimeNumber {

	/**
	 * @param args
	 *            updated
	 */

	static Logger logger = Logger.getLogger(logging.class.getName());

	public static void main(String[] args) {

		Logger logger = Logger.getLogger(logging.class.getName());
		printTest(10, 4);
		printTest(2, 2);
		printTest(54161329, 4);
		printTest(1882341361, 2);
		printTest(36, 9);

		logger.info(isPrime(54161329) + " expect false");
		logger.info(isPrime(1882341361) + " expect true");
		logger.info(isPrime(2) + " expect true");
	}

	public static boolean[] getPrimes(int max) {
		boolean[] result = new boolean[max + 1];
		for (int i = 2; i < result.length; i++) {
			result[i] = true;
		}

		final double LIMIT = Math.sqrt(max);
		for (int i = 2; i <= LIMIT; i++) {
			if (result[i]) {
				int index = 2 * i;
				while (index < result.length) {
					result[index] = false;
					index += i;
				}
			}
		}
		return result;
	}

	public static void printTest(int num, int expectedFactors) {
		int actualFactors = numFactors(num);
		logger.info("Testing " + num + " expect " + expectedFactors + ", " + "actual " + actualFactors);
		if (actualFactors == expectedFactors){
			logger.info("PASSED");
		}
		else{
			logger.info("FAILED");
		}
	}

	// Per: num >= 2
	public static boolean isPrime(int num) {
		if(num >= 2) {
			logger.info("failed precondition. num must be >= 2. num: " + num);
		}
		final double LIMIT = Math.sqrt(num);
		boolean isPrime = (num == 2);
		int div = 3;
		while (div <= LIMIT && isPrime) {
			isPrime = num % div != 0;
			div += 2;
		}
		return isPrime;
	}

	// pre: num >= 2
	public static int numFactors(int num) {
		if(num >= 2) {
			logger.info("failed precondition. num must be >= 2. num: " + num);
		}
		int result = 0;
		final double SQRT = Math.sqrt(num);
		for (int i = 1; i < SQRT; i++) {
			if (num % i == 0) {
				result += 2;
			}
		}
		if (num % SQRT == 0) {
			result++;
		}
		return result;
	}

}